
$(function () {
	//make connection
	var socket = io.connect('http://localhost:3000')

	//buttons and inputs
	var message = $("#message")
	var username = $("#username")
	var send_message = $("#send_message")
	var send_username = $("#send_username")
	var chatroom = $("#chatroom")
	var feedback = $("#feedback")

	//Emit message
	send_message.click(function () {
		socket.emit('new_message', { message: message.val() })
	})

	//Listen on new_message
	socket.on("new_message", (data) => {
		feedback.html('');
		message.val('');
		if (data.username === username.val()) {

			chatroom.append("<div class='me-container'><span class='username'></span><div class='message me'>" + data.message + "</div></div>")
		} else {
			chatroom.append("<div class='them-container'><div class='them-group'><span class='username'>" + data.username + "</span><div class='message them'>" + data.message + "</div>")
		}
	})

	//Emit a username
	send_username.click(function () {
		socket.emit('change_username', { username: username.val() })
	})

	//Emit typing
	message.bind("keypress", () => {
		socket.emit('typing')
	})

	//Listen on typing
	socket.on('typing', (data) => {
		feedback.html("<p>" + data.username + " is typing a message..." + "</p>")
	})
});


